3.1. Describe the last commit to the repository.

```
The last commit answered the `2-tooling` question
```

3.2. What was changed?

```
The text change for `2-tooling.md` file
```

3.3 What was the intention of this change?

```
Answer the question about tooling
```