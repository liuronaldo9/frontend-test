2.1. Explain the structure of the tooling.

```
src: The mian project and unit tests
babelrc: Babel confing
.eslint* : Eslint config
.nvmrc: Lock down a specific node version
prettier*: Prettier config
package.json: Minifest file for npm package info
package-lock: Current package version file
```

2.2. Describe the main packages that are used to build the project.

```
The project is mainly built in React, which is a Javascript library for building user interfaces, which is famous for its virtual dom system.
Also includes some dev packages, like babel: for transplanting ES6 to ES5, eslint: for preventing unexpected Javascript style or Syntax issue.
Jest: which is a testing framework which provides comprehensively support for unit test, contract test.
```

2.3. What are the main dot files (e.g. `.babelrc`) and what purpose do these serve?

```
The .babelrc file is the config file for babel which compiles code into a backwards compatible version of JavaScript, also provide powerful configuration for the individual project, work with module bundler tool to compress the code in a proper way.
```

2.4. Describe the tools used to statically analyse and test the code base. What is each tool's purpose?

```
Prettier: It removes all original styling and ensures that all outputted code conforms to a consistent style.
Eslint: static analysis that is frequently used to find problematic patterns or code that doesn’t adhere to certain style guidelines.
```

2.5. Describe how the tools are executed, and what would be a regular set of commands a developer would run.

```
yarn/npm test: Run unit test
yarn/npm start: Start the project
yarn/npm lint: Run eslint for javascript file under src dir
yarn/npm format: Run prettier for files for all dir
```

2.6. Bonus: feedback on the tools and structure. What doesn't make sense? What would you change?

```
put `components.html`, `design-mock.html`, `favourite-blue-button` at root dir which doesn't make sense, it might able to move into a doc folder under root dir.
others are great to me.
```