/**
 * These tests are pre-configured with Jest (https://jestjs.io/docs/en/getting-started),
 * and use sinon.js (https://sinonjs.org) for mocking, and React Testing Library
 * (https://testing-library.com/docs/react-testing-library/intro) for testing React
 * components. You are welcome to use these tools, replace with your preferred tools,
 * or write pseudo code to communicate your ideas.
 */
import 'react-testing-library/cleanup-after-each';
import React from 'react';
import Button from '../Button';
import sinon from 'sinon';
import { render, fireEvent } from 'react-testing-library';


describe('<Button/>', () => {
  it('should render as a button element when no type is provided', () => {
    const { container } = render(<Button />);
    expect(container).toMatchSnapshot();
  });

  it('should render as a link element when isLink flag is set', () => {
    const props = getProps();
    const { container } = render(<Button {...props}/>);
    expect(container).toMatchSnapshot();
    
    function getProps(){
      return {
        type: 'submit',
        isLink: true
      }
    }
  });

  it('should set the `target` prop as the `target` attribute if rendering a link', () => {
    const props = getProps();
    const { container } = render(<Button {...props}/>);
    expect(container).toMatchSnapshot();
    
    function getProps(){
      return {
        target: 'target',
        type: 'submit',
        isLink: true
      }
    }
  });

  it('should render as a link element with `rel="external noopener noreferrer"` if the `isExternalLink` prop is true', () => {
    const props = getProps();
    const { container } = render(<Button {...props}/>);
    expect(container).toMatchSnapshot();
    
    function getProps(){
      return {
        rel: 'external noopener noreferrer',
        target: 'target',
        isExternalLink: true,
        type: 'submit',
        isLink: true
      }
    }
  });

  it('should render as a link element with existing `rel` value intact when `isExternalLink` prop is false', () => {
    const props = getProps();
    const { container } = render(<Button {...props}/>);
    expect(container).toMatchSnapshot();
    
    function getProps(){
      return {
        rel: 'rel',
        isExternalLink: false,
        type: 'submit',
        isLink: true,
        target: 'target',
      }
    }
  });

  it('should be able to receive and handle an onClick callback', () => {
    const mockOnClick = jest.fn();
    const { container } = render(<Button onClick={mockOnClick} />);
    const button = container.querySelector('button');
    fireEvent.click(button);
    expect(mockOnClick).toHaveBeenCalled();
  });

  it('should support an href attribute if the button is a link and no click handler has been defined', () => {
    const props = getProps();
    const { container } = render(<Button {...props} />);
    expect(container).toMatchSnapshot();

    function getProps() {
      return {
        rel: 'rel',
        type: 'submit',
        isLink: true,
        target: 'target',
        isExternalLink: false,
        href: 'https://www.xero.com',
      };
    }
  });

  it('should be able to handle click events for links', () => {
    const mockOnClick = jest.fn();
    const props = getProps();
    const { container } = render(<Button {...props} />);
    const button = container.querySelector('a');
    fireEvent.click(button);
    expect(mockOnClick).toHaveBeenCalled();

    function getProps() {
      return {
        rel: 'rel',
        type: 'submit',
        isLink: true,
        target: 'target',
        isExternalLink: false,
        onClick: mockOnClick,
      };
    }
  });
});
